@extends('layouts.main_layout')

@section('content')
    <div class="col-xs-12 col-sm-12">
        <div class="jumbotron">
            <h1><b>Praćenje razvoja djece</b></h1>
            <p><b>Aplikacija za pomoć psiholozima u praćenju prilagodbe djece te njihovog rasta i napretka.
                Pisanje nalaza, organizacija rada, nadgledanje skupina i poremećaja koji se pojavljuju vrtićima.</b></p>
        </div>
    </div>
@endsection