<!DOCTYPE html>
<html lang="en">
<head>
  	<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
    <script>tinymce.init({ selector:'textarea' });</script>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    </head>
    <style>
        body {
            padding-top: 50px;
        }


        /*
         * Global add-ons
         */

        .sub-header {
            padding-bottom: 10px;
            border-bottom: 1px solid #eee;
        }

        /*
         * Top navigation
         * Hide default border to remove 1px line.
         */
        .navbar-fixed-top {
            border: 0;
        }

        /*
         * Sidebar
         */

        /* Hide for mobile, show later */
        .sidebar {
            display: none;
        }
        @media (min-width: 768px) {
            .sidebar {
                position: fixed;
                top: 51px;
                bottom: 0;
                left: 0;
                z-index: 1000;
                display: block;
                padding: 20px;
                overflow-x: hidden;
                overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
                background-color: #f5f5f5;
                border-right: 1px solid #eee;
            }
        }

        /* Sidebar navigation */
        .nav-sidebar {
            margin-right: -21px; /* 20px padding + 1px border */
            margin-bottom: 20px;
            margin-left: -20px;
        }
        .nav-sidebar > li > a {
            padding-right: 20px;
            padding-left: 20px;
        }
        .nav-sidebar > .active > a,
        .nav-sidebar > .active > a:hover,
        .nav-sidebar > .active > a:focus {
            color: #fff;
            background-color: #428bca;
        }


        /*
         * Main content
         */

        .main {
            padding: 20px;
        }
        @media (min-width: 768px) {
            .main {
                padding-right: 40px;
                padding-left: 40px;
            }
        }
        .main .page-header {
            margin-top: 0;
        }

        .poremecaj{
        	border: 1px solid #c4cad3;
        	text-align: center;
        	margin: 5px;
        	padding: 5px;
        }

        /*
         * Placeholder dashboard ideas
         */


    </style>

    <title>Dashboard</title>

<body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container-fluid">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style="color: white;">Djeca u vrticima</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li><a href="{{ url('index') }}">Odjava</a></li>
          </ul>
        </div>
      </div>
    </nav>

    <nav class = "navbar navbar-inverse navbar-left">
        <div class="col-sm-3 col-md-2 sidebar">
          <ul class="nav nav-sidebar sidebar-menu">
            <li class="" ><a href="{{ url('dashboard/nadzorna_ploca') }}"><i class="glyphicon glyphicon-dashboard"></i> Nadzorna ploča </a></li>
            <li class=""><a href="{{ url('dashboard/nalazi') }}"><i class="glyphicon glyphicon-comment"></i> Nalazi </a></li>
            <li class=""><a href="{{ url('dashboard/djeca') }}"><i class="glyphicon glyphicon-list"></i> Djeca </a></li>
            <li class=""><a href="{{ url('dashboard/poremecaji') }}"><i class="glyphicon glyphicon-book"></i> Poremećaji</a></li>
            <li class=""><a href="{{ url('dashboard/odgojiteljice') }}"><i class="glyphicon glyphicon-eye-open"></i> Odgojiteljice</a></li>
            <li class=""><a href="{{ url('dashboard/vrtici') }}"><i class="glyphicon glyphicon-home"></i> Vrtići </a></li>
            <li class=""><a href="{{ url('dashboard/novi_korisnik') }}"><i class="glyphicon glyphicon-user"></i> Novi korisnik </a></li>
          </ul>
        </div>
    </nav>

    <div class="container-fluid">
      <div class="row">
        @yield('content')
      </div>
    </div>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
        <script src="https://code.highcharts.com/highcharts.js"></script>
        <script src="https://code.highcharts.com/modules/exporting.js"></script>
  </body>
</html>
