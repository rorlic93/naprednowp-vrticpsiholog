@extends('layouts.main_layout')

@section('content')
    <div class="col-xs-12 col-sm-12">
        <div class="jumbotron">
            <h1><b>Niste prijavljeni</b></h1>
            <p><b>Molimo prijavite se s vašim korisničkim imenom i lozinkom</b></p>
        </div>
    </div>
@endsection
