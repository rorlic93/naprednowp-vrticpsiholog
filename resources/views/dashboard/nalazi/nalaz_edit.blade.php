@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h2>Unos novog nalaza</h2>
        	  <p>Molimo popunite sve točke</p>

        	  @if($data['isUpdated'])
                  <div class="alert alert-success" style="margin-top: 20px;">
                      <strong>Uspjeh!</strong> Nalaz je uspješno ažuriran
                  </div>
              @endif

        	  <form action="{{route('nalazi.nalaz_edit')}}" method="POST">
        	  {{csrf_field()}}
        	  <fieldset>
        	    <div class="form-group">
        	    	<label for="dijete">Dijete:</label>
        			<br/>
        			<select name="dijete" class="form-control" style="margin-top: 10px;" name="dijete">
        			@foreach($data['dijete'] as $dijete)
        				<option value="{{$dijete->id}}">{{$dijete->ime . ' ' . $dijete->prezime}}</option>
                    @endforeach
        			</select>
        	    </div>
        	    <div class="form-group">
        	    	<label for="poremecaj">Poremećaj:</label>
        			<br/>
        			<select name="poremecaj" class="form-control" style="margin-top: 10px;" name="poremecaj">
                    @foreach($data['poremecaj'] as $poremecaj)
                        <option value="{{$poremecaj->id}}">{{$poremecaj->naziv}}</option>
                    @endforeach
        			</select>
        	    </div>
        	    <div class="form-group">
        	    	<label for="psiholog">Psiholog odgovoran za nalaz:</label>
        			<br/>
        			<input class="form-control" id="disabledInput" type="text" name="psiholog" value="{{$data['psiholog']}}" disabled/>
        	    </div>
        	    <div class="form-group">
        	      <label for="sadrzaj">Sadržaj:</label>
        	      <textarea name="sadrzaj" id="sadrzaj" rows="10" cols="80">{{$data['sadrzaj']}}</textarea>
        	    </div>
        	    <div class="form-group">
        	      <input class="form-control btn btn-success" type="submit" name="insert" value="Pohrani"/>
        	      <input type="button" class="form-control btn btn-danger" data-toggle="modal" data-target="#myModal" value="Izbriši"/>
                  <input type="button" class="form-control btn btn-default" onclick="window.location='{{ url('dashboard/nalazi') }}'" value="Nazad"/>
        	    </div>
        	  </fieldset>
        	  </form>

        	  <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Brisanje nalaza</h4>
                      </div>
                      <div class="modal-body">
                        Jeste li sigurni da želite obrisati nalaz?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
                        <button type="button" class="btn btn-primary"
                        onclick="window.location='{{ url('dashboard/nalazi/nalaz_delete/' . $data['id']) }}'"  >
                        Obriši
                        </button>
                      </div>
                    </div>
                  </div>
              </div>

        	  {{--<p>{{$data['dijete'] . '  ||  ' . $data['poremecaj']}}</p>--}}
</div>
