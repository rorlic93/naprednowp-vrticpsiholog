@extends('...layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h2>Unos novog nalaza</h2>
    	  <p>Molimo popunite sve točke</p>

    	  @if($data['id'] != null)
    	  <div class="alert alert-success" style="margin-top: 20px;">
            <strong>Uspjeh!</strong> Nalaz je unesen u bazu
          </div>
          @endif

    	  <form action ="{{route('nalazi.nalaz_insert') }}" method="POST">
    	  {{csrf_field()}}
    	  <fieldset>
    	    <div class="form-group">
    	    	<label for="dijete">Dijete:</label>
    			<br/>
    			<select name="dijete" class="form-control" style="margin-top: 10px;" name="dijete">
    			@foreach($data['djeca'] as $red)
    				<option value='{{$red->id}}'><?php echo $red->prezime . " " . $red->ime; ?></option>
                @endforeach
    			</select>
    	    </div>
    	    <div class="form-group">
    	    	<label for="poremecaj">Poremećaj:</label>
    			<br/>
    			<select name="poremecaj" class="form-control" style="margin-top: 10px;" name="poremecaj">
    				<option>Nije zapažen nijedan poremećaj</option>
    				@foreach($data['poremecaji'] as $red)
    				<option  value='{{$red->id}}'>{{$red->naziv}}</option>
    				@endforeach
    			</select>
    	    </div>
    	    <div class="form-group">
    	    	<label for="psiholog">Psiholog odgovoran za nalaz:</label>
    			<br/>
    			<input class="form-control" id="disabledInput" type="text" name="psiholog" value='{{$data['psiholog']}}' disabled/>
    	    </div>
    	    <div class="form-group">
    	      <label for="sadrzaj">Sadržaj:</label>
    	      <textarea name="sadrzaj" id="sadrzaj" rows="10" cols="80"></textarea>
    	    </div>
    	    <div class="form-group">
    	      <input class="form-control btn btn-success" type="submit" name="insert" value="Pohrani"/>
    	    </div>
          </fieldset>
    	  </form>

    	  {{--<p>{{$data['now'] . ' ! ' . $data['sadzaj'] . ' - ' . $data['poremecaj'] . ' / ' . $data['dijete']}}</p>--}}

</div>