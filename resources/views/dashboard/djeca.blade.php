@extends('layouts.dashboard_layout')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">Djeca</h1>
    <div class="btn-group btn-group-justified">
    	<a href="{{ url('dashboard/djeca/dijete_insert') }}" class="btn btn-success">Unos</a>
      	<a href="{{ url('dashboard/djeca/dijete_find') }}" class="btn btn-warning">Pretraga</a>
    </div>
    <hr>
    <h3 class="sub-header">Djeca s ustanovljenim poremećajima</h3>
    <div class="table-responsive">
    <table class="table table-hover" >
      <thead style="text-align: right;">
        <tr >
          <th class="text-center">ID</th>
          <th class="text-center">Ime</th>
          <th class="text-center">Prezime</th>
          <th class="text-center">Dob</th>
          <th class="text-center">Zabilježeni poremećaj</th>
          <th class="text-center">Skupina</th>
          <th class="text-center">Vrtić</th>
          <th class="text-center">Akcija</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data['djeca'] as $item)
        <tr style="text-align: center;">
          <td>{{$item->id}}</td>
          <td>{{$item->ime}}</td>
          <td>{{$item->prezime}}</td>
          <td>{{$item->dob}}</td>
          <td>{{$item->naziv}}</td>
          <td>{{$item->ime_skupine}}</td>
          <td>{{$item->ime_vrtica}}</td>
          <td>
          <button class="btn btn-primary">
      		<a href="{{url('dashboard/djeca/dijete_info/' . $item->id)}}" style="text-decoration: none; color: white;">Pregled </a>
      	  </button>
      	  <button class="btn btn-danger">
      		<a data-toggle="modal" data-target="#myModal" style="text-decoration: none; color: white;">Obriši </a>
      	  </button>
      	  </td>
        </tr>

        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Brisanje djeteta</h4>
                      </div>
                      <div class="modal-body">
                        Jeste li sigurni da želite obrisati dijete iz baze?
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
                        <button type="button" class="btn btn-primary"
                        onclick="window.location='{{ url('dashboard/djeca/dijete_delete/' . $item->id) }}'"  >
                        Obriši
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
      @endforeach
      </tbody>
    </table>
    </div>

    </div>