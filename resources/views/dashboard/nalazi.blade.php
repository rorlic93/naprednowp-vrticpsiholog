@extends('layouts.dashboard_layout')

@section('content')
    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
      <h1 class="page-header">Nalazi</h1>

      <div class="btn-group btn-group-justified" >
      	<a href="{{ url('dashboard/nalazi/nalaz_insert') }}" class="btn btn-success">Novi nalaz</a>
        <a href="{{ url('dashboard/nalazi/nalaz_find') }}" class="btn btn-warning">Pretraži nalaze</a>
      </div>

      <h3 class="sub-header">Zadnjih 20 napisanih nalaza</h3>
      <div class="table-responsive">
      <table class="table table-striped" >
        <thead style="text-align: right;">
          <tr >
            <th class="text-center">ID</th>
            <th class="text-center">Ime</th>
            <th class="text-center">Prezime</th>
            <th class="text-center">Vrijeme zadnjeg nalaza</th>
            <th class="text-center">Poremećaj</th>
            <th class="text-center">Vrtić</th>
            <th class="text-center">Akcija</th>
          </tr>
        </thead>
        <tbody>
        @foreach($data['djeca'] as $item)
          <tr style="text-align: center;">
            <td>{{$item->id}}</td>
            <td>{{$item->ime}}</td>
            <td>{{$item->prezime}}</td>
            <td>{{$item->updated_at}}</td>
            <td>{{$item->naziv}}</td>
            <td>{{$item->ime_vrtica}}</td>
            <td>
            <button class="btn btn-primary">
        		<a href="{{ url('dashboard/nalazi/nalaz_edit/' . $item->id)}}" style="text-decoration: none; color: white;">Uredi </a>
        	  </button>
        	  </td>
          </tr>
        @endforeach
        </tbody>

      </table>
      </div>

    </div>