@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h2><b>IME I PREZIME</b></h2>


    <h2>Nalazi</h2>
    <hr/>
    <div class="row centered">
    @foreach($data['nalazi'] as $item)
    	<div class="border col-xs-3 nalaz" style="max-width: 33.3%; height: 220px; border: 1px solid black; margin: 5px; text-align: center; box-shadow: 5px 5px 2px #888888;">
    		<h4><b>Nalaz </b></h4>
    		<p style="margin-top: 10px;">Poremećaj: </p>
    		<b><p>{{$item->naziv}}</p></b>
    		<p>Datum nalaza: </p>
    		<b><p>{{$item->updated_at}}</p></b>
    		<button class="btn btn-primary" style="position:absolute; bottom:0px; margin:5px; left:0px; "><a href="{{ url('dashboard/nalazi/nalaz_edit/' . $data['id'])}}" style="color: #FFFFFF">Uredi</a></button>
    		<button class="btn btn-danger" style="position:absolute; bottom:0px; margin:5px; right:0px; "><a data-toggle="modal" data-target="#myModal" style="color: #FFFFFF">Izbriši</a></button>
    	</div>

    	<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
          <div class="modal-dialog" role="document">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Brisanje nalaza</h4>
              </div>
              <div class="modal-body">
                Jeste li sigurni da želite obrisati nalaz?
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Odustani</button>
                <button type="button" class="btn btn-primary"
                onclick="window.location='{{ url('dashboard/nalazi/nalaz_delete/' . $data['id']) }}'"  >
                Obriši
                </button>
              </div>
            </div>
          </div>
        </div>


    @endforeach
    </div>

    <h2>Poremećaji</h2>
    <hr/>
    <div class="row centered">
    @foreach($data['poremecaji'] as $item)
    	<div class="border col-xs-3 poremecaj" style="max-width: 33.3%; height: 100px; border: 1px solid black; margin: 5px; text-align: center; box-shadow: 5px 5px 2px #888888;">
    		<p style="margin-top: 10px;"><i>Poremećaj </i></p>
    		<b><p>{{$item->naziv}}</p></b>
    	</div>

    @endforeach
    </div>
</div>