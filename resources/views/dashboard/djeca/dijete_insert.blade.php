@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h2>Unos novog djeteta</h2>
    	  <p>Molimo popunite sve točke</p>

    	  @if($data['id'] != null)
            <div class="alert alert-success" style="margin-top: 20px;">
              <strong>Uspjeh!</strong> Dijete je uneseno u bazu
            </div>
          @endif

    	  <form action="{{route('dijete_insert')}}" method="post">
    	  {{csrf_field()}}
    	    <div class="form-group">
    	    	<label for="ime">Ime:</label>
    			<br/>
    			@if($data['ime'] == null)
    			    <input class="form-control" type="text" name="ime" value=""/>
    			@else
                    <input class="form-control" type="text" name="ime" value="{{$data['ime']}}"/>
    			@endif
    	    </div>
    	    <div class="form-group">

    	    	<label for="prezime">Prezime:</label>
    			<br/>
    			@if($data['prezime'] == null)
                    <input class="form-control" type="text" name="prezime" value=""/>
                @else
                    <input class="form-control" type="text" name="prezime" value="{{$data['prezime']}}"/>
                @endif
    	    </div>
    	    <div class="form-group">
    	    	<label for="prezime">Dob:</label>
    			<br/>
    			<select class="form-control" style="margin-top: 10px;" name="dob">
    			@for($i = 1; $i < 7; $i++)
    				@if($data['dob'] == $i)
                        <option value="{{$i}}" selected="selected">{{$i}}</option>
                    @else
                        <option value="{{$i}}">{{$i}}</option>
    				@endif
                @endfor
    			</select>
    	    </div>
    	    <div class="form-group">
    	    	<label for="poremecaj">Odaberite poremećaj</label>
    			<br/>
    			<select class="form-control" style="margin-top: 10px;" name="poremecaj">
    			    <option value="0">Nema zabiljezen poremecaj</option>
    			@foreach($data['poremecaji'] as $item)
    			    @if($data['selectedPId'] == $item->id)
                        <option value="{{$item->id}}" selected="selected">{{$item->naziv}}</option>
    			    @else
    			        <option value="{{$item->id}}">{{$item->naziv}}</option>
    			    @endif
                @endforeach
    			</select>
    	    </div>
    	    <div class="form-group">
    	    	<label for="skupina">Vrtić:</label>
    			<br/>
    			<select class="form-control" style="margin-top: 10px;" name="vrtic" onchange="this.form.submit()">
                    @foreach($data['vrtici'] as $item)
                        @if($item->id == $data['selectedId'])
                            <option value="{{$item->id}}" selected="selected">{{$item->ime_vrtica}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->ime_vrtica}}</option>
                        @endif
                    @endforeach
    			</select>
    	    </div>
    	    <div class="form-group">
    	    	<label for="skupina">Skupina:</label>
    			<br/>
    			<select class="form-control" style="margin-top: 10px;" name="skupina" >
                    <option value="0">...</option>
                    @if($data['isSelected'])
                    @foreach($data['skupine'] as $item)
                        @if($item->id == $data['selectedSId'])
                            <option value="{{$item->id}}" selected="selected">{{$item->ime_skupine}}</option>
                        @else
                            <option value="{{$item->id}}">{{$item->ime_skupine}}</option>
                        @endif
                    @endforeach
                    @endif
    			</select>
    	    </div>
    	    <div class="form-group">
    	    @if($data['id'] != null)
    	        <input type="button" class="form-control btn btn-default" onclick="window.location='{{ url('dashboard/nalazi') }}'" value="Nazad"/>
    	    @else
    	        <input class="form-control btn btn-success" type="submit" name="insert" value="Pohrani"/>
    	    @endif

    	    </div>
    	  </form>

    	  <p>{{$data['isSelected'] . ' || ' . $data['skupine'] . ' || ' . $data['selectedId']}}</p>
</div>