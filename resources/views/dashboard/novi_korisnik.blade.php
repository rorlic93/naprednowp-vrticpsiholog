@extends('layouts.dashboard_layout')

@section('content')

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    <h2>Unos novog psihologa (admina)</h2>
    	<p>Molimo popunite sve točke</p>

    	@if($data['isCreated'] != null && $data['isCreated'])
          <div class="alert alert-success" style="margin-top: 20px;">
            <strong>Uspjeh!</strong> Korisnik je unesen u bazu
          </div>
        @endif

    	<form action="{{route('korisnik')}}" method="post">
    	{{csrf_field()}}
            <div class="form-group">
                <label for="ime">Ime:</label>
                <br/>
                <input type="text" class="form-control" name="ime"/>
            </div>
            <div class="form-group">
                <label for="prezime">Prezime:</label>
                <br/>
                <input type="text" class="form-control" name="prezime"/>
            </div>
            <div class="form-group">
                <label for="email">Email:</label>
                <br/>
                <input type="text" class="form-control" name="email"/>
            </div>
            <div class="form-group">
                <label for="lozinka">Lozinka:</label>
                <br/>
                <input type="password" class="form-control" name="password"/>
            </div>
            <div class="form-group">
                <label for="lozinka_2">Potvrdi lozinku:</label>
                <br/>
                <input type="password" class="form-control" name="password2"/>
            </div>
            <div class="form-group">
              <input class="form-control btn btn-success" type="submit" name="insert" value="Pohrani"/>
            </div>
        </form>

        @if($data['isValidated'] != null && !$data['isValidated'])
          <div class="alert alert-danger" style="margin-top: 20px;">
            <strong>Oprez!</strong> Provjerite istovjetnost unesenih lozinki!
          </div>
        @endif
</div>