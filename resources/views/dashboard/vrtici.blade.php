@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1>Popis vrtića</h1>
    <hr/>
    <div class="table-responsive">
    	<table class="table table-striped" >
    	  <thead style="text-align: right;">
    	    <tr>
    	      <th class="text-center">Ime vrtića</th>
    	      <th class="text-center">Adresa</th>
    	      <th class="text-center">Akcija</th>
    	    </tr>
    	  </thead>
    	  <tbody>
    	  @foreach($data['vrtici'] as $item)
    	    <tr style="text-align: center;">
    	      <td>{{$item->ime_vrtica}}</td>
    	      <td>{{$item->adresa}}</td>
    	      <td>
    	      <button class="btn btn-primary">
    	  		<a href="{{ url('dashboard/vrtici/vrtic_info/' . $item->id) }}" style="text-decoration: none; color: #FFFFFF;">Pregled </a>
    	  	  </button>
    	  	  </td>
    	    </tr>
    	  @endforeach
    	  </tbody>

    	</table>
    </div>

</div>