@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h2>Unos nove odgojiteljice</h2>
    	<p>Molimo popunite sve točke</p>
    	  <form method="post">
    	    <div class="form-group">
    	    	<label for="ime">Ime:</label>
    			<br/>
    			<input type="text" name="ime" class="form-control" value=""/>
    	    </div>
    	    <div class="form-group">
    	    	<label for="prezime">Prezime:</label>
    			<br/>
    			<input type="text" name="prezime" class="form-control" value=""/>
    	    </div>
    	    <div class="form-group">
    	    	<label for="skupina">Vrtić:</label>
    			<br/>
    			<select class="form-control" style="margin-top: 10px;" name="vrtic" onchange="this.form.submit()">
    				<option value="">
    				</option>
    			</select>
    	    </div>
    	    <div class="form-group">
    	      <label for="skupina">Skupina:</label>
    	      <select class="form-control" style="margin-top: 10px;" name="skupina">

    	      	<option value=""></option>
    	      </select>
    	    </div>
    	    <div class="form-group">
    	      <input class="form-control btn btn-success" type="submit" name="insert" value="Pohrani"/>
    	    </div>
    	  </form>
</div>