@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h2>Pretraga odgojiteljica</h2>
    	<form method="POST">
    	{{csrf_field()}}
    		<label for="uvjet">Unesite uvjet(ime, prezime, skupina, vrtić):</label><br/>
    		<input class="form-control" type="text" name="uvjet" style="margin-top: 10px; margin-bottom: 10px;"  value="" />

    		<input class="form-control btn btn-success" type="submit" name="find" value="PRETRAGA"/>
    	</form>

    	<div>
    		<div class="alert alert-info" style="margin-top: 20px;">
    		<strong>Rezultati za pojam: {{$data['pojam']}}</strong>
    		    <p style="font-size: 10px; margin-top: 5px;">Poredano abecednim redom prezimena</p>
    		</div>
    	</div>

    <div class="table-responsive">
    <table class="table table-hover" >
        <thead style="text-align: right;">
    	    <tr>
    	      <th class="text-center">Prezime</th>
    	      <th class="text-center">Ime</th>
    	      <th class="text-center">Skupina</th>
    	      <th class="text-center">Vrtić</th>
    	    </tr>
        </thead>
        <tbody>
          @foreach($data['odgojiteljice'] as $item)
        	<tr style="text-align: center;">
    	        <td>{{$item->prezime}}</td>
    	        <td>{{$item->ime}}</td>
    	        <td>{{$item->ime_skupine}}</td>
    	        <td>{{$item->ime_vrtica}}</td>
            </tr>
          @endforeach
        </tbody>
    </table>
    </div>
</div>