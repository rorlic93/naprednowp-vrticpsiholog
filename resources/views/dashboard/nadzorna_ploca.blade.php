@extends('layouts.dashboard_layout')

@section('content')
<style>
    body {
        padding-top: 0px;
    }
</style>

    <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

      <h1 class="page-header">Nadzorna ploča</h1>

        <h2 class="sub-header">Djeca - zadnjih 20 nalaza</h2>
        <div class="table-responsive">
          <table class="table table-striped" >
            <thead style="text-align: right;">
              <tr >
                <th class="text-center">#</th>
                <th class="text-center">Ime</th>
                <th class="text-center">Prezime</th>
                <th class="text-center">Vrijeme zadnjeg nalaza</th>
                <th class="text-center">Poremećaj</th>
                <th class="text-center">Vrtić</th>
                <th class="text-center">Akcija</th>
              </tr>
            </thead>
            <tbody>
            @foreach($data['djeca'] as $item)
              <tr style="text-align: center;">
                <td>{{$item->id}}</td>
                <td>{{$item->ime}}</td>
                <td>{{$item->prezime}}</td>
                <td>{{$item->updated_at}}</td>
                <td>{{$item->naziv}}</td>
                <td>{{$item->ime_vrtica}}</td>
                <td>
                <button type="button" class="btn btn-primary" onclick="window.location='{{ url('dashboard/nalazi/nalaz_edit/' . $item->id) }}'">
                  Pregled
                </button>
                </td>
              </tr>
            @endforeach
            </tbody>

          </table>
        </div>

      <hr>
      <div>
            <footer>
              <p>&copy; 2017 Napredno web programiranje, Hajduković-Lekić-Orlić</p>
            </footer>
      </div>
    </div>
    <div id="djecaskupine" data-field-id="{{$data['djeca_skupine']}}" ></div>
    <div id="poremecajiskupine" data-field-id="{{$data['poremecaji_skupine']}}" ></div>
    <div id="djecavrtici" data-field-id="{{$data['djeca_vrtici']}}" ></div>

@endsection
