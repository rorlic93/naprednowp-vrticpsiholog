@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">

    @foreach($data['skupina'] as $item)
    <h1><b><i>{{$item->ime_skupine}}</i></b> | <b>{{$item->ime_vrtica}}</b></h1>
    @endforeach
    <h3>Odgojiteljice:</h3>
    <p>
        @foreach($data['odgojiteljice'] as $item)
            	{{$item->ime . ' ' . $item->prezime . ' | ' }}
        @endforeach
    </p>
    <h3>Djeca: </h3>
    <div class="table-responsive">
    	<table class="table table-striped" >
    	  <thead style="text-align: right;">
    	    <tr>
    	      <th class="text-center">Dijete</th>
    	      <th class="text-center">Dob</th>
    	      <th class="text-center">Poremećaj</th>
    	      <th class="text-center">Akcija</th>
    	    </tr>
    	  </thead>
    	  <tbody>
    	  @foreach($data['djeca'] as $item)
    	    <tr style="text-align: center;">
    	      <td>{{$item->ime . ' ' . $item->prezime}}</td>
    	      <td>{{$item->dob}}</td>
    	      <td>{{$item->naziv}}</td>
    	      <td>
    	      <button class="btn btn-primary">
      		  	<a href="{{url('dashboard/djeca/dijete_info/' . $item->id)}}" style="text-decoration: none; color: white;">Pregled </a>
      	      </button>
      	      </td>
    	    </tr>
    	  @endforeach
    	  </tbody>

    	</table>
    </div>
</div>