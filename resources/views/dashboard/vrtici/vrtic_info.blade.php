@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h2>Skupine u vrtiću <b><i>{{$data['ime_vrtica']}}</i></b></h2>
    <div class="table-responsive">
    	<table class="table table-striped" >
    	  <thead style="text-align: right;">
    	    <tr>
    	      <th class="text-center">Ime skupine</th>
    	    </tr>
    	  </thead>
    	  <tbody>
    	  @foreach($data['skupine'] as $item)
    	    <tr style="text-align: center;">
    	      <td><a href="{{ url('dashboard/vrtici/skupina_info/' . $item->id) }}">{{$item->ime_skupine}}</a></td>
    	    </tr>
    	  @endforeach
    	  </tbody>
    	</table>
    </div>

</div>