@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <script>
        $(".poremecaj").hover(function(){
            $(this).css("background-color", "lightblue");
            }, function(){
            $(this).css("background-color", "#FFF");
        });

    </script>

    <h1 class="page-header">Poremećaji</h1>
    <div class="btn-group btn-group-justified">
        <a href="{{ url('dashboard/poremecaji/poremecaj_insert') }}" class="btn btn-info">Unos novog poremećaja</a>
    </div>
    <hr>
    @foreach($data['poremecaji'] as $item)
      <div class="col-xs-3 poremecaj" style="height: 180px; box-shadow: 5px 5px 2px #888888">
        <b><p>{{$item->naziv}}</p></b>
        <p>Broj djece s poremećajem: </p>
        <b><p>{{$item->n}}</p></b>
        <button class="btn btn-warning" style="position:absolute; bottom:0px; margin:5px; left:0px; ">
            <a href="{{ url('dashboard/poremecaji/poremecaj_edit/' . $item->id) }}" style="color: #FFFFFF">Uredi</a>
        </button>
        <button class="btn btn-info" style="position:absolute; bottom:0px; margin:5px; right:0px; ">
            <a href="{{ url('dashboard/poremecaji/poremecaj_info/' . $item->id) }}" style="color: #FFFFFF">Pregled</a>
        </button>
      </div>
    @endforeach
</div>