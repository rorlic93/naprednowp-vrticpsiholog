@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h2>Uređivanje</h2>
    	<p>Molimo popunite sve točke</p>
    	  <form action="{{route('poremecaj_update')}}"  method="post">
    	  {{csrf_field()}}
    	  @foreach($data['poremecaj'] as $item)
    	    <div class="form-group">
    	    	<label for="naziv">Naziv:</label>
    			<br/>
    			<input type="text" class="form-control" name="naziv" value="{{$item->naziv}}" />
    	    </div>
    	    <div class="form-group">
    	    	<label for="opis" >Opis:</label>
    			<br/>
    			<input type="text" name="opis" class="form-control" value="{{$item->opis}}" />
    	    </div>
    	    <div class="form-group">
    	      <label for="terapija">Terapija:</label>
    	      <textarea name="terapija" rows="10" cols="80">{{$item->terapija}}</textarea>
    	    </div>
    	    <div class="form-group">
    	      <input class="form-control btn btn-success" type="submit" name="insert" value="Pohrani"/>
    	      <input class="form-control btn btn-default" value="Odustani" onclick="window.location='{{ url('dashboard/poremecaji') }}'"/>
    	    </div>
    	  @endforeach
    	  </form>
</div>