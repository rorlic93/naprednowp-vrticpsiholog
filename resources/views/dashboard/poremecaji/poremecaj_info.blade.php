@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <div class="alert alert-info" style="margin-top: 20px;">
    @foreach($data['poremecaj'] as $item)
    	<h2><strong>Pregled poremećaja:</strong> <i>{{$item->naziv}}</i></h2>
    @endforeach
    </div>

    <hr/>
    <h2>Djeca s navedenim poremećajem: </h2>
    <div class="table-responsive">
    	<table class="table table-striped" >
    	  <thead style="text-align: right;">
    	    <tr>
    	      <th class="text-center">#</th>
    	      <th class="text-center">Prezime</th>
    	      <th class="text-center">Ime</th>
    	      <th class="text-center">Akcija</th>
    	    </tr>
    	  </thead>
    	  <tbody>
    	  @foreach($data['djeca'] as $item)
    	    <tr style="text-align: center;">
    	      <td>{{$item->id}}</td>
    	      <td>{{$item->ime}}</td>
    	      <td>{{$item->prezime}}</td>
    	      <td>
    	      <button class="btn btn-primary">
    	  		<a href="{{url('dashboard/djeca/dijete_info/' . $item->id)}}" style="text-decoration: none; color: #FFFFFF;">Pregled </a>
    	  	  </button>
    	  	  </td>
    	    </tr>
    	  </tbody>
          @endforeach
    	</table>
    </div>
</div>