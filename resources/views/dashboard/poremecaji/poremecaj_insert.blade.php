@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h2>Unos novog poremećaja</h2>
    	<p>Molimo popunite sve točke</p>

    	@if($data['id'] != null)
            <div class="alert alert-success" style="margin-top: 20px;">
              <strong>Uspjeh!</strong> Poremecaj je unesen u bazu
            </div>
        @endif

    	  <form action="{{route('poremecaj_insert')}}" method="post">
    	  {{csrf_field()}}
    	    <div class="form-group">
    	    	<label for="naziv">Naziv:</label>
    			<br/>
    			<input type="text" class="form-control" name="naziv"/>
    	    </div>
    	    <div class="form-group">
    	    	<label for="opis" >Opis:</label>
    			<br/>
    			<input type="text" name="opis" class="form-control"/>
    	    </div>
    	    <div class="form-group">
    	      <label for="terapija">Terapija:</label>
    	      <textarea name="terapija" rows="10" cols="80"></textarea>
    	    </div>
    	    <div class="form-group">
    	      <input class="form-control btn btn-success" type="submit" name="insert" value="Pohrani"/>
    	    </div>
    	  </form>
</div>