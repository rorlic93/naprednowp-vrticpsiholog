@extends('layouts.dashboard_layout')

@section('content')
<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
    <h1 class="page-header">Odgojiteljice</h1>
    <div class="btn-group btn-group-justified">
    	<a href="{{ url('dashboard/odgojiteljice/odgojiteljica_find') }}" class="btn btn-warning">Pretraga</a>
    </div>
    <hr/>
    <h2>Popis zadnjih 20 unešenih odgojiteljica</h2>
    <div class="table-responsive">
    <table class="table table-striped" >
      <thead style="text-align: right;">
        <tr >
          <th class="text-center">ID</th>
          <th class="text-center">Odgojiteljica</th>
          <th class="text-center">Skupina</th>
          <th class="text-center">Vrtić</th>
        </tr>
      </thead>
      <tbody>
      @foreach($data['odgojiteljice'] as $item)
        <tr style="text-align: center;">
          <td>{{$item->id}}</td>
          <td>{{$item->ime . ' ' . $item->prezime}}</td>
          <td>{{$item->ime_skupine}}</td>
          <td>{{$item->ime_vrtica}}</td>
        </tr>
      @endforeach
      </tbody>
    </table>
    </div>
</div>