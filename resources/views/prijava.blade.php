@extends('layouts.main_layout')

@section('content')
    <div class="col-xs-12 col-sm-12" >
        <div class="jumbotron" style="background-color: rgba(222,226,232,0.3);">
            @if (count($errors) > 0)
                @foreach($errors->all as $error)
                    <p class="alert alert-danger" >{{$error}}</p>
                @endforeach
            @endif
            <form style="padding: 5px; opacity: 1;" action={{route('prijava')}} method="POST">
             {{ csrf_field() }}
                <fieldset>
                    <label style="padding-bottom: 5px;"><b>Email:</b></label></br>
                    <input type="email" name="email" placeholder="example@example.com" value="{{old('email')}}" class="form-control"/>
                    <br/>
                    <label style="margin-top: 5px;" ><b>Password:</b></label></br>
                    <input  type="password" name="password" placeholder="yeti4preZ" value="{{old('password')}}" class="form-control" />
                    </br>

                    <input style="margin-top: 5px;" type="submit" value="Login" class = "btn btn-success" />
                </fieldset>
            </form>

         </div>
    </div>
@endsection