<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class poremecaj extends Model
{
    //

    protected $table = 'poremecaj';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'naziv', 'opis', 'terapija'];

    public function dijete(){
        return $this->belongsToMany('App\dijete','dijete_poremecaj', 'dijete', 'poremecaj');
    }

    public function nalaz(){
        return $this->hasMany('App\nalaz');
    }
}
