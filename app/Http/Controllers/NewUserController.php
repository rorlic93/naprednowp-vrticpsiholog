<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class NewUserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $currentUser = Session::get('user');
        if($currentUser == null){
          return redirect('not_logged_in');
        }

        $data['isCreated'] = null;
        $data['isValidated'] = null;

        return view('dashboard.novi_korisnik')->withData($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $now = Carbon::now();

        if($request->password == $request->password2){
            $isValidated = true;
            $idU = DB::table('users')->insertGetId(
                ['email' => $request->email, 'password' => Hash::make('$request->password'), 'created_at' => $now, 'updated_at' => $now]
            );
            if($idU != null){
                $isCreated = true;
            }else{
                $isCreated = false;
            }
        }else{
            $isValidated = false;
            $isCreated = false;
        }

        $data['isCreated'] = $isCreated;
        $data['isValidated'] = $isValidated;


        return view('dashboard.novi_korisnik')->withData($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
