<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class NadzornaPlocaController extends Controller
{
    public function showViewWithData(){

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $djeca = DB::table('dijete')
            ->join('nalaz', 'dijete.id', '=', 'nalaz.dijete')
            ->join('poremecaj', 'nalaz.poremecaj', '=', 'poremecaj.id')
            ->join('skupina', 'dijete.skupina', '=', 'skupina.id')
            ->join('vrtic', 'skupina.vrtic', '=', 'vrtic.id')
            ->orderBy('nalaz.updated_at', 'desc')->take(20)
            ->select('dijete.id', 'dijete.ime', 'dijete.prezime', 'nalaz.updated_at', 'poremecaj.naziv', 'vrtic.ime_vrtica')
            ->get();

        $omjer1 = DB::table('dijete')
            ->join('skupina', 'dijete.skupina', '=', 'skupina.id')
            ->select('skupina.ime_skupine', DB::raw("count(dijete.id) as ukupno"))
            ->groupBy('skupina.ime_skupine')
            ->get();

        $array = array();
        foreach ($omjer1 as $red):
            $info = array("name" => $red->ime_skupine, "y" => (int)$red->ukupno);
            array_push($array, $info);
        endforeach;

        $djeca_skupine = json_encode($array);

        $omjer2 = DB::table('skupina')
            ->join('dijete', 'skupina.id', '=', 'dijete.skupina')
            ->join('dijete_poremecaj', 'dijete.id', '=', 'dijete_poremecaj.dijete')
            ->join('poremecaj', 'dijete_poremecaj.poremecaj', '=', 'poremecaj.id')
            ->select('skupina.ime_skupine', DB::raw("count(poremecaj.naziv) as ukupno"))
            ->groupBy('skupina.ime_skupine')
            ->get();

        $array_poremecaj_skupina = array();
        foreach($omjer2 as $linija):
            $info = array("name"=>$linija->ime_skupine, "y"=>(int)$linija->ukupno);
            array_push($array_poremecaj_skupina, $info);
        endforeach;

        $poremecaji_skupine = json_encode($array_poremecaj_skupina);

        $omjer3 = DB::table('vrtic')
            ->join('skupina', 'vrtic.id', '=', 'skupina.vrtic')
            ->join('dijete', 'skupina.id', '=', 'dijete.skupina')
            ->select('vrtic.ime_vrtica', DB::raw("count(dijete.ime) as ukupno"))
            ->groupBy('vrtic.ime_vrtica')
            ->get();

        $array_djeca_vrtic = array();
        foreach($omjer3 as $linija):
            $podaci = array("name"=>$linija->ime_vrtica, "y"=>(int)$linija->ukupno);
            array_push($array_djeca_vrtic, $podaci);
        endforeach;

        $djeca_vrtici = json_encode($array_djeca_vrtic);

        $data['djeca'] = $djeca;
        $data['djeca_skupine'] = $djeca_skupine;
        $data['poremecaji_skupine'] = $poremecaji_skupine;
        $data['djeca_vrtici'] = $djeca_vrtici;

        return view('dashboard.nadzorna_ploca', compact('djeca_skupine', 'poremecaji_skupine', 'djeca_vrtici'))->withData($data);
    }
}
