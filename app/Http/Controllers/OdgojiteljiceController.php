<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\psiholog;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\dijete;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class OdgojiteljiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $odgojiteljice = DB::table('odgojiteljica')
            ->join('skupina', 'odgojiteljica.skupina', '=', 'skupina.id')
            ->join('vrtic', 'skupina.vrtic', '=', 'vrtic.id')
            ->select('odgojiteljica.id', 'odgojiteljica.ime', 'odgojiteljica.prezime', 'skupina.ime_skupine', 'vrtic.ime_vrtica')
            ->get();

        $data['odgojiteljice'] = $odgojiteljice;

        return view('dashboard.odgojiteljice')->withData($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $odgojiteljice = DB::table('odgojiteljica')
            ->join('skupina', 'odgojiteljica.skupina', '=', 'skupina.id')
            ->join('vrtic', 'skupina.vrtic', '=', 'vrtic.id')
            ->select('odgojiteljica.id', 'odgojiteljica.ime', 'odgojiteljica.prezime', 'skupina.ime_skupine', 'vrtic.ime_vrtica')
            ->get();

        $data['pojam'] = ' ';
        $data['odgojiteljice'] = $odgojiteljice;
        return view('dashboard.odgojiteljice.odgojiteljica_find')->withData($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showSearchResult(Request $request)
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $stringToFind = $request->uvjet;
        $uvjet = "%" . $stringToFind . "%";

        $odgojiteljice = DB::table('odgojiteljica')
            ->join('skupina', 'odgojiteljica.skupina', '=', 'skupina.id')
            ->join('vrtic', 'skupina.vrtic', '=', 'vrtic.id')
            ->select('odgojiteljica.id', 'odgojiteljica.ime', 'odgojiteljica.prezime', 'skupina.ime_skupine', 'vrtic.ime_vrtica')
            ->where('odgojiteljica.ime', 'like', $uvjet)
            ->orWhere('odgojiteljica.prezime', 'like', $uvjet)
            ->orWhere('skupina.ime_skupine', 'like', $uvjet)
            ->orWhere('vrtic.ime_vrtica', 'like', $uvjet)
            ->get();

        $data['pojam'] = $stringToFind;
        $data['odgojiteljice'] = $odgojiteljice;
        return view('dashboard.odgojiteljice.odgojiteljica_find')->withData($data);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
