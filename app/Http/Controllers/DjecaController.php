<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\psiholog;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\dijete;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class DjecaController extends Controller
{

    public function index()
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $djeca = DB::table('dijete')
            ->join('dijete_poremecaj', 'dijete.id', '=', 'dijete_poremecaj.dijete')
            ->join('poremecaj', 'dijete_poremecaj.poremecaj', '=', 'poremecaj.id')
            ->join('skupina', 'dijete.skupina', '=', 'skupina.id')
            ->join('vrtic', 'skupina.vrtic', '=', 'vrtic.id')
            ->orderBy('dijete.id', 'desc')->take(20)
            ->select('dijete.id', 'dijete.ime', 'dijete.prezime', 'dijete.dob', 'skupina.ime_skupine', 'poremecaj.naziv', 'vrtic.ime_vrtica')
            ->get();

        $data['djeca'] = $djeca;
        return view('dashboard.djeca')->withData($data);
    }

    public function create()
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $data['isSelected'] = false;

        $poremecaji = DB::table('poremecaj')
            ->select('poremecaj.id', 'poremecaj.naziv')
            ->orderBy('poremecaj.naziv', 'asc')
            ->get();

        $vrtici = DB::table('vrtic')
            ->select('vrtic.id', 'vrtic.ime_vrtica')
            ->orderBy('vrtic.ime_vrtica', 'asc')
            ->get();

        $skupine = null;

        $data['ime'] = null;
        $data['prezime'] = null;
        $data['dob'] = null;
        $data['poremecaji'] = $poremecaji;
        $data['vrtici'] = $vrtici;
        $data['skupine'] = $skupine;
        $data['selectedId'] = null;
        $data['selectedPId'] = null;
        $data['selectedSId'] = null;
        $data['id'] = null;

        return view('dashboard.djeca.dijete_insert')->withData($data);
    }

    public function store(Request $request)
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        if($request->skupina == 0){

            $poremecaji = DB::table('poremecaj')
                ->select('poremecaj.id', 'poremecaj.naziv')
                ->orderBy('poremecaj.naziv', 'asc')
                ->get();

            $vrtici = DB::table('vrtic')
                ->select('vrtic.id', 'vrtic.ime_vrtica')
                ->orderBy('vrtic.ime_vrtica', 'asc')
                ->get();

            $skupine = DB::table('skupina')
                ->select('skupina.id', 'skupina.ime_skupine')
                ->where('skupina.vrtic', '=', $request->vrtic)
                ->orderBy('skupina.ime_skupine', 'asc')
                ->get();

            $data['ime'] = $request->ime;
            $data['prezime'] = $request->prezime;
            $data['dob'] = $request->dob;
            $data['isSelected'] = true;
            $data['selectedPId'] = $request->poremecaj;
            $data['selectedSId'] = null;
            $data['poremecaji'] = $poremecaji;
            $data['vrtici'] = $vrtici;
            $data['skupine'] = $skupine;
            $data['selectedId'] = $request->vrtic;
            $data['id'] = null;

            return view('dashboard.djeca.dijete_insert')->withData($data);
        }else{

            $now = Carbon::now();

            $id = DB::table('dijete')->insertGetId(
                ['ime' => $request->ime, 'prezime' => $request->prezime, 'dob' => $request->dob, 'skupina' => $request->skupina, 'created_at' => $now, 'updated_at' => $now]
            );

            if($request->poremecaj != 0){
                DB::table('dijete_poremecaj')->insert(
                    ['dijete' => $id, 'poremecaj' => $request->poremecaj, 'created_at' => $now, 'updated_at' => $now]
                );
            }

            $poremecaji = DB::table('poremecaj')
                ->select('poremecaj.id', 'poremecaj.naziv')
                ->where('poremecaj.id', '=', $request->poremecaj)
                ->orderBy('poremecaj.naziv', 'asc')
                ->get();

            $vrtici = DB::table('vrtic')
                ->select('vrtic.id', 'vrtic.ime_vrtica')
                ->where('vrtic.id', '=', $request->vrtic)
                ->orderBy('vrtic.ime_vrtica', 'asc')
                ->get();

            $skupine = DB::table('skupina')
                ->select('skupina.id', 'skupina.ime_skupine')
                ->where('skupina.id', '=', $request->skupina)
                ->orderBy('skupina.ime_skupine', 'asc')
                ->get();

            $data['ime'] = $request->ime;
            $data['prezime'] = $request->prezime;
            $data['dob'] = $request->dob;
            $data['isSelected'] = true;
            $data['poremecaji'] = $poremecaji;
            $data['vrtici'] = $vrtici;
            $data['skupine'] = $skupine;
            $data['selectedId'] = $request->vrtic;
            $data['selectedPId'] = $request->poremecaj;
            $data['selectedSId'] = $request->skupina;
            $data['id'] = $id;

            return view('dashboard.djeca.dijete_insert')->withData($data);
        }
    }

    public function showWithoutData()
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $djeca = DB::table('dijete')
            ->join('dijete_poremecaj', 'dijete.id', '=', 'dijete_poremecaj.dijete')
            ->join('poremecaj', 'dijete_poremecaj.poremecaj', '=', 'poremecaj.id')
            ->join('skupina', 'dijete.skupina', '=', 'skupina.id')
            ->join('vrtic', 'skupina.vrtic', '=', 'vrtic.id')
            ->orderBy('dijete.id', 'desc')->take(20)
            ->select('dijete.id', 'dijete.ime', 'dijete.prezime', 'dijete.dob', 'skupina.ime_skupine', 'poremecaj.naziv', 'vrtic.ime_vrtica')
            ->get();

        $data['djeca'] = $djeca;
        $data['pojam'] = ' ';

        return view('dashboard.djeca.dijete_find')->withData($data);
    }

    public function showWithData(Request $request){

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $stringToFind = $request->uvjet;
        $uvjet = "%" . $stringToFind . "%";

        $djeca = DB::table('dijete')
            ->join('dijete_poremecaj', 'dijete.id', '=', 'dijete_poremecaj.dijete')
            ->join('poremecaj', 'dijete_poremecaj.poremecaj', '=', 'poremecaj.id')
            ->join('skupina', 'dijete.skupina', '=', 'skupina.id')
            ->join('vrtic', 'skupina.vrtic', '=', 'vrtic.id')
            ->orderBy('dijete.id', 'desc')->take(20)
            ->select('dijete.id', 'dijete.ime', 'dijete.prezime', 'dijete.dob', 'skupina.ime_skupine', 'poremecaj.naziv', 'vrtic.ime_vrtica')
            ->where('dijete.ime', 'like', $uvjet)
            ->orWhere('dijete.prezime', 'like', $uvjet)
            ->get();

        $data['djeca'] = $djeca;
        $data['pojam'] = $stringToFind;

        return view('dashboard.djeca.dijete_find')->withData($data);
    }

    public function info($id)
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $poremecaji = DB::table('poremecaj')
            ->join('dijete_poremecaj', 'poremecaj.id', '=', 'dijete_poremecaj.poremecaj')
            ->orderBy('poremecaj.naziv')
            ->select('poremecaj.naziv')
            ->where('dijete_poremecaj.dijete', '=', $id)
            ->get();

        $nalazi = DB::table('nalaz')
            ->join('dijete', 'nalaz.dijete', '=', 'dijete.id')
            ->join('dijete_poremecaj', 'dijete_poremecaj.dijete', '=', 'dijete.id')
            ->join('poremecaj', 'dijete_poremecaj.poremecaj', '=', 'poremecaj.id')
            ->orderBy('nalaz.updated_at', 'desc')
            ->select('nalaz.id', 'nalaz.updated_at', 'poremecaj.naziv')
            ->where('nalaz.dijete', '=', $id)
            ->get();

        $data['poremecaji'] = $poremecaji;
        $data['nalazi'] = $nalazi;
        $data['id'] = $id;

        return view('dashboard.djeca.dijete_info')->withData($data);
    }

    public function update(Request $request, $id)
    {

    }

    public function destroy($id)
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        DB::table('dijete')->where('dijete.id', '=', $id)->delete();
        DB::table('dijete_poremecaj')->where('dijete_poremecaj.dijete', '=', $id)->delete();


        return redirect('dashboard/djeca');
    }
}
