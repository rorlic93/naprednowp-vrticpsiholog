<?php

namespace App\Http\Controllers;
use App\psiholog;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\dijete;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class NalaziController extends Controller
{

    public function showViewWithData(){

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $djeca = DB::table('dijete')
            ->join('nalaz', 'dijete.id', '=', 'nalaz.dijete')
            ->join('poremecaj', 'nalaz.poremecaj', '=', 'poremecaj.id')
            ->join('skupina', 'dijete.skupina', '=', 'skupina.id')
            ->join('vrtic', 'skupina.vrtic', '=', 'vrtic.id')
            ->orderBy('nalaz.updated_at', 'desc')->take(20)
            ->select('dijete.id', 'dijete.ime', 'dijete.prezime', 'nalaz.updated_at', 'poremecaj.naziv', 'vrtic.ime_vrtica')
            ->get();

        $data['djeca'] = $djeca;

        return view('dashboard.nalazi')->withData($data);
    }

    public function presentInsertFormWithData(){

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $djeca = DB::table('dijete')
            ->orderBy('dijete.prezime', 'asc')
            ->select('dijete.prezime', 'dijete.ime', 'dijete.id')
            ->get();

        $poremecaji = DB::table('poremecaj')
            ->orderBy('poremecaj.naziv')
            ->select('poremecaj.id', 'poremecaj.naziv')
            ->get();

        $currentUser = Session::get('user');
        $psiholog = psiholog::where('email', '=', $currentUser)->get();

        $data['djeca'] = $djeca;
        $data['poremecaji'] = $poremecaji;
        $data['psiholog'] = $currentUser;

        $data['now'] = ' ';
        $data['sadzaj'] = ' ';
        $data['poremecaj'] = ' ';
        $data['dijete'] = ' ';
        $data['id'] = null;
        return view('dashboard.nalazi.nalaz_insert')->withData($data);
    }

    public function insert(Request $request){

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $djeca = DB::table('dijete')
            ->orderBy('dijete.prezime', 'asc')
            ->select('dijete.prezime', 'dijete.ime', 'dijete.id')
            ->get();

        $poremecaji = DB::table('poremecaj')
            ->orderBy('poremecaj.naziv')
            ->select('poremecaj.id', 'poremecaj.naziv')
            ->get();

        $currentUser = Session::get('user');
        $psiholog = psiholog::where('email', '=', $currentUser)->get();

        $data['djeca'] = $djeca;
        $data['poremecaji'] = $poremecaji;
        $data['psiholog'] = $currentUser;

        $now = Carbon::now();
        $id = DB::table('nalaz')->insertGetId(
            ['dijete' => $request->dijete, 'poremecaj' => $request->poremecaj, 'created_at' => $now, 'updated_at' => $now, 'sadrzaj' => $request->sadrzaj, 'psiholog' => 1]
        );

        DB::table('dijete_poremecaj')->insert(
            ['dijete' => $request->dijete, 'poremecaj' => $request->poremecaj, 'created_at' => $now, 'updated_at' => $now]
        );

        $data['now'] = $now;
        $data['sadzaj'] = $request->sadrzaj;
        $data['poremecaj'] = $request->poremecaj;
        $data['dijete'] = $request->dijete;
        $data['id'] = $id;


        return view('dashboard.nalazi.nalaz_insert')->withData($data);
    }

    public function presentFindViewWithoutData(){

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $uvjet = ' ';

        $djeca = DB::table('dijete')
            ->join('nalaz', 'dijete.id', '=', 'nalaz.dijete')
            ->join('poremecaj', 'nalaz.poremecaj', '=', 'poremecaj.id')
            ->orderBy('nalaz.updated_at', 'desc')
            ->select('dijete.ime', 'dijete.prezime', 'poremecaj.naziv', 'nalaz.updated_at', 'dijete.id')
            ->get();

        $data['djeca'] = $djeca;
        $data['pojam'] = $uvjet;

        return view('dashboard.nalazi.nalaz_find')->withData($data);
    }

    public function find(Request $request){

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $this->validate($request, [
            'uvjet' => 'required'
        ]);

        $stringToFind = $request->uvjet;

        $uvjet = "%" . $stringToFind . "%";

        $djeca = DB::table('dijete')
            ->join('nalaz', 'dijete.id', '=', 'nalaz.dijete')
            ->join('poremecaj', 'nalaz.poremecaj', '=', 'poremecaj.id')
            ->select('dijete.ime', 'dijete.prezime', 'poremecaj.naziv', 'nalaz.updated_at', 'dijete.id')
            ->where('dijete.ime', 'like', $uvjet)
            ->orWhere('poremecaj.naziv', 'like', $uvjet)
            ->get();

        $data['djeca'] = $djeca;
        $data['pojam'] = $stringToFind;

        return view('dashboard.nalazi.nalaz_find')->withData($data);
    }

    public function presentEditViewWithData($id){

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $dijete = DB::table('dijete')
            ->join('nalaz', 'dijete.id', '=', 'nalaz.dijete')
            ->select('dijete.prezime', 'dijete.ime', 'dijete.id', 'nalaz.sadrzaj')
            ->where('dijete.id', '=', $id)
            ->get();

        $poremecaj = DB::table('dijete_poremecaj')
            ->join('dijete', 'dijete_poremecaj.dijete', '=', 'dijete.id')
            ->join('poremecaj', 'dijete_poremecaj.poremecaj', '=', 'poremecaj.id')
            ->select('poremecaj.naziv', 'poremecaj.id')
            ->where('dijete_poremecaj.dijete', '=', $id)
            ->get();

        $nalaz = DB::table('nalaz')
            ->select('nalaz.sadrzaj')
            ->where('nalaz.dijete', '=', $id)
            ->get();

        foreach($nalaz as $item){
            $sadrzaj = $item->sadrzaj;
        }

        $currentUser = Session::get('user');
        $psiholog = psiholog::where('email', '=', $currentUser)->get();

        $data['dijete'] = $dijete;
        $data['poremecaj'] = $poremecaj;
        $data['psiholog'] = $currentUser;
        $data['sadrzaj'] = $sadrzaj;
        $data['id'] = $id;
        $data['isUpdated'] = false;

        return view('dashboard.nalazi.nalaz_edit')->withData($data);
    }

    public function put(Request $request){

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $dijete = DB::table('dijete')
            ->join('nalaz', 'dijete.id', '=', 'nalaz.dijete')
            ->select('dijete.prezime', 'dijete.ime', 'dijete.id', 'nalaz.sadrzaj')
            ->where('dijete.id', '=', $request->dijete)
            ->get();

        $poremecaj = DB::table('dijete_poremecaj')
            ->join('dijete', 'dijete_poremecaj.dijete', '=', 'dijete.id')
            ->join('poremecaj', 'dijete_poremecaj.poremecaj', '=', 'poremecaj.id')
            ->select('poremecaj.naziv', 'poremecaj.id')
            ->where('dijete_poremecaj.dijete', '=', $request->dijete)
            ->get();


        $now = Carbon::now();
        $currentUser = Session::get('user');

        DB::table('nalaz')
            ->where('nalaz.dijete', $request->dijete)
            ->update(['sadrzaj' => $request->sadrzaj, 'updated_at' => $now]);

        $data['dijete'] = $dijete;
        $data['poremecaj'] = $poremecaj;
        $data['psiholog'] = $currentUser;
        $data['sadrzaj'] = $request->sadrzaj;
        $data['id'] = $request->dijete;
        $data['isUpdated'] = true;

        return view('dashboard.nalazi.nalaz_edit')->withData($data);
    }

    public function delete($id){

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        DB::table('nalaz')->where('nalaz.dijete', '=', $id)->delete();

        return redirect('dashboard/nalazi');
    }

}
