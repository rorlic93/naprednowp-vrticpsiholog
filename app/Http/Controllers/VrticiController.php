<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

class VrticiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $vrtici = DB::table('vrtic')
            ->select('vrtic.ime_vrtica', 'vrtic.adresa', 'vrtic.id')
            ->get();

        $data['vrtici'] = $vrtici;

        return view('dashboard.vrtici')->withData($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $skupine = DB::table('vrtic')
            ->join('skupina', 'vrtic.id', '=', 'skupina.vrtic')
            ->select('skupina.id', 'skupina.ime_skupine')
            ->where('skupina.vrtic', '=', $id)
            ->groupBy('skupina.id', 'skupina.ime_skupine')
            ->get();

        $vrtic = DB::table('vrtic')->select('vrtic.ime_vrtica')->where('vrtic.id', '=', $id)->get();

        $data['skupine'] = $skupine;
        foreach($vrtic as $item){
            $data['ime_vrtica'] = $item->ime_vrtica;
        }

        return view('dashboard.vrtici.vrtic_info')->withData($data);
    }

    public function showGroupInfo($id){

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $skupina = DB::table('skupina')
            ->join('vrtic', 'skupina.id', '=', 'vrtic.id')
            ->select('skupina.ime_skupine', 'vrtic.ime_vrtica')
            ->where('skupina.id', '=', $id)
            ->get();

        $odgojiteljice = DB::table('odgojiteljica')
            ->join('skupina', 'odgojiteljica.skupina', '=', 'skupina.id')
            ->select('odgojiteljica.ime', 'odgojiteljica.prezime')
            ->where('odgojiteljica.skupina', '=', $id)
            ->get();

        $djeca = DB::table('dijete')
            ->join('dijete_poremecaj', 'dijete.id', '=', 'dijete_poremecaj.dijete')
            ->join('poremecaj', 'dijete_poremecaj.poremecaj', '=', 'poremecaj.id')
            ->select('dijete.id', 'dijete.ime', 'dijete.prezime', 'dijete.dob', 'poremecaj.naziv')
            ->where('dijete.skupina', '=', $id)
            ->get();

        $data['skupina'] = $skupina;
        $data['odgojiteljice'] = $odgojiteljice;
        $data['djeca'] = $djeca;

        return view('dashboard.vrtici.skupina_info')->withData($data);
    }

}
