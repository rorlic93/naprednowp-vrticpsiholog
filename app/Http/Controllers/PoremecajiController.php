<?php

namespace App\Http\Controllers;

use App\dijete_poremecaj;
use Illuminate\Http\Request;
use App\psiholog;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\dijete;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class PoremecajiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $poremecaji = DB::table('poremecaj')
            ->join('dijete_poremecaj', 'poremecaj.id', '=', 'dijete_poremecaj.poremecaj')
            ->select(DB::raw('COUNT(dijete_poremecaj.poremecaj) as n'), 'poremecaj.naziv', 'poremecaj.id')
            ->orderBy('poremecaj.naziv', 'asc')
            ->groupBy('dijete_poremecaj.poremecaj', 'poremecaj.naziv', 'poremecaj.id')
            ->get();

        $data['poremecaji'] = $poremecaji;

        return view('dashboard.poremecaji')->withData($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $id = null;

        $data['id'] = $id;
        return view('dashboard.poremecaji.poremecaj_insert')->withData($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $now = Carbon::now();

        $id = DB::table('poremecaj')->insertGetId(
            ['naziv' => $request->naziv, 'opis' => $request->opis, 'terapija' => $request->terapija, 'created_at' => $now, 'updated_at' => $now]
        );
        $data['id'] = $id;
        return view('dashboard.poremecaji.poremecaj_insert')->withData($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $poremecaj = DB::table('poremecaj')
            ->select('poremecaj.naziv')
            ->where('poremecaj.id', '=', $id)
            ->get();

        $data['poremecaj'] = $poremecaj;

        $djeca = DB::table('dijete')
            ->join('dijete_poremecaj', 'dijete.id', '=', 'dijete_poremecaj.dijete')
            ->join('poremecaj', 'poremecaj.id', '=', 'dijete_poremecaj.poremecaj')
            ->select('dijete.ime', 'dijete.prezime', 'poremecaj.naziv', 'dijete.id')
            ->orderBy('dijete.prezime')
            ->where('poremecaj.id', '=', $id)
            ->get();

        $data['djeca'] = $djeca;

        return view('dashboard.poremecaji.poremecaj_info')->withData($data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        $poremecaj = DB::table('poremecaj')
            ->select('poremecaj.naziv', 'poremecaj.opis', 'poremecaj.terapija')
            ->where('poremecaj.id', '=', $id)
            ->get();

        $data['poremecaj'] = $poremecaj;
        $data['id'] = $id;

        return view('dashboard.poremecaji.poremecaj_edit')->withData($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

      $currentUser = Session::get('user');
      if($currentUser == null){
        return redirect('not_logged_in');
      }

        DB::table('poremecaj')
            ->where('naziv', '=', $request->naziv)
            ->update(
                ['naziv' => $request->naziv, 'opis' => $request->opis, 'terapija' => $request->terapija]
            );


        return redirect('dashboard/poremecaji');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
