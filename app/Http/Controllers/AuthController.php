<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function showAuthForm(){
        return view('prijava');
    }

    public function authenticateUser(Request $request){
        $this->validate($request, [
            'email' => 'required|email|max:255',
            'password' => 'required'
        ]);
        if(Auth::attempt(['email' => $request->email, 'password' => $request->password])){
            Session::put('user', $request->email);
            return redirect('dashboard/nadzorna_ploca');
        }else{
            return redirect('error');
        }
    }
}
