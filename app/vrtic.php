<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class vrtic extends Model
{
    //
    protected $table = 'vrtic';
    protected $primaryKey = 'id';
    protected $fillable = ['id', 'ime_vrtica', 'adresa', 'N_djece', 'N_skupina', 'psiholog'];

    public function skupina(){
        return $this->hasMany('App\skupina');
    }
}
