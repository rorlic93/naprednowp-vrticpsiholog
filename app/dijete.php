<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dijete extends Model
{
    protected $table = 'dijete';
    protected $primaryKey = 'id';

    protected $fillable = ['ime', 'prezime', 'dob', 'skupina'];

    public function skupina(){
        return $this->belongsTo('App\skupina');
    }

    public function poremecaj(){
        return $this->belongsToMany('App\poremecaj', 'dijete_poremecaj', 'dijete', 'poremecaj');
    }

    public function nalaz(){
        return $this->hasMany('App\nalaz');
    }
}
