<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/index', function () {
    return view('index');
});


Route::get('/psiholog', function(){
    return view('psiholog');
});

Route::get('/era', function(){
    return view('era');
});

Route::get('prijava', 'AuthController@showAuthForm')->name('prijava');
Route::post('prijava', 'AuthController@authenticateUser')->name('prijava');

Route::get('/error', function(){
    return view('error');
});

Route::get('/not_logged_in', function(){
    return view('not_logged_in');
});

//NADZORNA PLOCA...
Route::get('dashboard/nadzorna_ploca', 'NadzornaPlocaController@showViewWithData')->name('nadzorna_ploca');

//NALAZI
Route::get('dashboard/nalazi', 'NalaziController@showViewWithData')->name('nalazi');

Route::get('dashboard/nalazi/nalaz_insert', 'NalaziController@presentInsertFormWithData')->name('nalazi.nalaz_insert_show');
Route::post('dashboard/nalazi/nalaz_insert', 'NalaziController@insert')->name('nalazi.nalaz_insert');

Route::get('dashboard/nalazi/nalaz_edit/{id}', 'NalaziController@presentEditViewWithData')->name('nalazi.nalaz_edit_show');
Route::post('dashboard/nalazi/nalaz_edit', 'NalaziController@put')->name('nalazi.nalaz_edit');

Route::get('dashboard/nalazi/nalaz_delete/{id}', 'NalaziController@delete')->name('nalazi.nalaz_delete');

Route::get('dashboard/nalazi/nalaz_find', 'NalaziController@presentFindViewWithoutData')->name('nalazi.nalaz_find_show');
Route::post('dashboard/nalazi/nalaz_find', 'NalaziController@find')->name('nalazi.nalaz_find');

//DJECA...
Route::get('dashboard/djeca', 'DjecaController@index')->name('djeca');
//CREATE
Route::get('dashboard/djeca/dijete_insert', 'DjecaController@create')->name('dijete_insert_show');
Route::post('dashboard/djeca/dijete_insert', 'DjecaController@store')->name('dijete_insert');
//FIND
Route::get('dashboard/djeca/dijete_find', 'DjecaController@showWithoutData')->name('dijete_find_show');
Route::post('dashboard/djeca/dijete_find', 'DjecaController@showWithData')->name('dijete_find');
//DELETE
Route::get('dashboard/djeca/dijete_delete/{id}', 'DjecaController@destroy')->name('dijete_delete');
//INFO
Route::get('dashboard/djeca/dijete_info/{id}', 'DjecaController@info')->name('dijete_info');

//POREMECAJI...
Route::get('dashboard/poremecaji', 'PoremecajiController@index')->name('poremecaji');
//INSERT
Route::get('dashboard/poremecaji/poremecaj_insert', 'PoremecajiController@create')->name('poremecaj_insert_show');
Route::post('dashboard/poremecaji/poremecaj_insert', 'PoremecajiController@store')->name('poremecaj_insert');
//UPDATE
Route::get('dashboard/poremecaji/poremecaj_edit/{id}', 'PoremecajiController@edit')->name('poremecaj_edit_show');
Route::post('dashboard/poremecaji/poremecaj_edit}', 'PoremecajiController@update')->name('poremecaj_update');
//INFO
Route::get('dashboard/poremecaji/poremecaj_info/{id}', 'PoremecajiController@show')->name('poremecaj_info');

//ODGOJITELJICE...
Route::get('dashboard/odgojiteljice', 'OdgojiteljiceController@index')->name('odgojiteljice');

Route::get('dashboard/odgojiteljice/odgojiteljica_find', 'OdgojiteljiceController@show')->name('odgojiteljice_find');
Route::post('dashboard/odgojiteljice/odgojiteljica_find', 'OdgojiteljiceController@showSearchResult')->name('odgojiteljice_find');

//VRTICI...
Route::get('dashboard/vrtici', 'VrticiController@index')->name('vrtici');
Route::get('dashboard/vrtici/vrtic_info/{id}', 'VrticiController@show')->name('vrtic_info');
Route::get('dashboard/vrtici/skupina_info/{id}', 'VrticiController@showGroupInfo')->name('vrtic_info');

//NOVI KORISNIK
Route::get('dashboard/novi_korisnik', 'NewUserController@index')->name('korisnik_show');
Route::post('dashboard/novi_korisnik', 'NewUserController@create')->name('korisnik');



Route::get('/', function () {
    return view('welcome');
});
